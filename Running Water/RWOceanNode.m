//
//  RWOceanNode.m
//  Running Water
//
//  Created by Ryan Salton on 19/01/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import "RWOceanNode.h"
#import "RWOceanBackgroundNode.h"

@interface RWOceanNode ()

@property (nonatomic, strong) RWOceanBackgroundNode *backgroundBG1;
@property (nonatomic, strong) RWOceanBackgroundNode *backgroundBG2;
@property (nonatomic, strong) RWOceanBackgroundNode *backgroundBG3;

@end

@implementation RWOceanNode

- (instancetype)initWithSize:(CGSize)size
{
    self = [super init];
    
    if (self)
    {
        SKSpriteNode *oceanBlue = [SKSpriteNode spriteNodeWithColor:[UIColor colorWithRed:0.0 green:0.6 blue:1.0 alpha:1.0] size:CGSizeMake(size.width, size.height)];
        oceanBlue.position = CGPointMake(size.width * 0.5, size.height * 0.5);
        oceanBlue.alpha = 0.7;
        [self addChild:oceanBlue];
        
        oceanBlue.zPosition = 10;
        
        [self createBackgroundWithSize:size];
    }
        
    return self;
}

- (void)createBackgroundWithSize:(CGSize)size
{
    self.backgroundBG1 = [RWOceanBackgroundNode node];
    self.backgroundBG2 = [RWOceanBackgroundNode node];
    self.backgroundBG3 = [RWOceanBackgroundNode node];
    
    [self addChild:self.backgroundBG1];
    [self addChild:self.backgroundBG2];
    [self addChild:self.backgroundBG3];
    
    self.backgroundBG1.sceneSize = self.backgroundBG2.sceneSize = self.backgroundBG3.sceneSize = size;
    
    self.backgroundBG1.sceneSpeed = 10.0;
    self.backgroundBG2.sceneSpeed = 20.0;
    self.backgroundBG3.sceneSpeed = 30.0;
    
    self.backgroundBG1.sceneScale = 0.4;
    self.backgroundBG2.sceneScale = 0.7;
    self.backgroundBG3.sceneScale = 1.0;
    
    self.backgroundBG1.zPosition = 1;
    self.backgroundBG2.zPosition = 2;
    self.backgroundBG3.zPosition = 3;
}

@end
