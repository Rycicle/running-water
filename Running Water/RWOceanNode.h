//
//  RWOceanNode.h
//  Running Water
//
//  Created by Ryan Salton on 19/01/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface RWOceanNode : SKNode

- (instancetype)initWithSize:(CGSize)size;

@end
