//
//  AppDelegate.h
//  Running Water
//
//  Created by Ryan Salton on 19/01/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

