//
//  RWOceanBackgroundNode.h
//  Running Water
//
//  Created by Ryan Salton on 20/01/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface RWOceanBackgroundNode : SKNode

@property (nonatomic, assign) CGFloat sceneSpeed;
@property (nonatomic, assign) CGFloat sceneScale;
@property (nonatomic, assign) CGSize sceneSize;

@end
