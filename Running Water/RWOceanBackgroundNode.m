//
//  RWOceanBackgroundNode.m
//  Running Water
//
//  Created by Ryan Salton on 20/01/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import "RWOceanBackgroundNode.h"

@implementation RWOceanBackgroundNode

- (void)setSceneSpeed:(CGFloat)speed
{
    _sceneSpeed = speed;
    
    [self removeAllActions];
    
    [self runAction:[SKAction repeatActionForever:[SKAction moveByX:-speed y:0 duration:1.0]]];
}

- (void)setSceneScale:(CGFloat)scale
{
    _sceneScale = scale;
    [self generateSpritesWithScale:scale];
}

- (void)generateSpritesWithScale:(CGFloat)scale
{
//    if (scale == self.sceneScale)
    {
        NSLog(@"%f", self.position.x);
        
        NSInteger randInt = arc4random_uniform(20);
        
        if (randInt == 1)
        {
            SKSpriteNode *sprite = [SKSpriteNode spriteNodeWithColor:[UIColor yellowColor] size:CGSizeMake(70 * scale, 70 * scale)];
            sprite.position = CGPointMake(self.sceneSize.width - self.position.x + (sprite.size.width * 0.5), arc4random_uniform(self.sceneSize.height - (sprite.size.height * 0.5)));
            [self addChild:sprite];
        }
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self generateSpritesWithScale:scale];
        });
        
    }
}

@end
