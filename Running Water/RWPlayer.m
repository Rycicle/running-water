//
//  RWPlayer.m
//  Running Water
//
//  Created by Ryan Salton on 19/01/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import "RWPlayer.h"

@implementation RWPlayer

+ (instancetype)spriteNodeWithColor:(UIColor *)color size:(CGSize)size
{
    RWPlayer *player = [RWPlayer spriteNodeWithColor:color size:size];
    
    return player;
}

@end
