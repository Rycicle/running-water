//
//  RWGameScene.m
//  Running Water
//
//  Created by Ryan Salton on 19/01/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import "RWGameScene.h"
#import "RWPlayer.h"
#import "RWOceanNode.h"

@interface RWGameScene ()

@property (nonatomic, assign) CGPoint playerOriginalPosition;

@property (nonatomic, strong) RWPlayer *player;
@property (nonatomic, strong) RWOceanNode *ocean;

@end

@implementation RWGameScene

-(void)didMoveToView:(SKView *)view {
    
    // set up background elements
    
    self.backgroundColor = [UIColor colorWithRed:0.5 green:0.9 blue:1.0 alpha:1.0];
    self.physicsWorld.gravity = CGVectorMake(0.0, -1.0);
    
//     create player
    
    self.player = [[RWPlayer alloc] initWithColor:[UIColor redColor] size:CGSizeMake(60, 60)];
    self.player.position = CGPointMake((self.player.size.width * 0.5) + 30, self.size.height * 0.5);
    self.playerOriginalPosition = self.player.position;
    self.player.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:self.player.size];
    self.player.physicsBody.mass = 0.175;
    self.player.physicsBody.restitution = 0.0;
    [self addChild:self.player];
    
    // create playerStop
    
    SKSpriteNode *floor = [SKSpriteNode spriteNodeWithColor:[UIColor clearColor] size:CGSizeMake(self.size.width, 10)];
    floor.position = CGPointMake(self.size.width * 0.5, self.player.position.y - (self.player.size.height * 2.0));
    floor.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:floor.size];
    floor.physicsBody.restitution = 0.0;
    floor.physicsBody.dynamic = NO;
    [self addChild:floor];
    
    // create ocean
    
    self.ocean = [[RWOceanNode alloc] initWithSize:CGSizeMake(self.size.width, self.player.position.y - (self.player.size.height * 0.5))];
    self.ocean.physicsBody = [SKPhysicsBody bodyWithEdgeLoopFromRect:self.frame];
    self.ocean.physicsBody.dynamic = NO;
    [self addChild:self.ocean];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self];
        if (self.player.position.y <= self.playerOriginalPosition.y)
        {
            [self.player.physicsBody applyImpulse:CGVectorMake(0.0, 2.5)];
        }
    }
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
    
}

@end
